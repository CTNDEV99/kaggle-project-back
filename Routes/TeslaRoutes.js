const express = require('express');
const router = express.Router();
const teslaController = require('../Controllers/TeslaController');

/**
 * @swagger
 * api/tesla/save:
 *   get:
 *     summary: Enregistrer tous les donnees issus du fichier tsla_raw_data.csv
 *     description: REnregistrer tous les donnees issus du fichier tsla_raw_data.csv.
 *     responses:
 *       200:
 *         description: Enregistrer tous les donnees issus du fichier tsla_raw_data.csv
 *       500:
 *         description: An error occurred
 */
router.get('/save', teslaController.saveData);
/**
 * @swagger
 * api/tesla/kpi:
 *   post:
 *     summary: Recup des statistiques
 *     description: Recup des statistiques
 *     responses:
 *       200:
 *         description: Recup des statistiques
 *       500:
 *         description: An error occurred
 */
router.post('/kpi', teslaController.getKpi);
/**
 * @swagger
 * api/tesla/int:
 *   get:
 *     summary: Avoir la date la plus ancienne et la plus recentes
 *     description: Avoir la date la plus ancienne et la plus recentes
 *     responses:
 *       200:
 *         description: Avoir la date la plus ancienne et la plus recentes
 *       500:
 *         description: An error occurred
 */
router.get('/int', teslaController.intDate);

module.exports = router;