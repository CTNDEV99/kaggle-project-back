const mongoose = require('mongoose');

const teslaSchema = new mongoose.Schema({
  date: { type: Date, required: true, unique: true },
  open: { type: Number, required: true},
  high: { type: Number, required: true},
  low: { type: Number, required: true,},
  close: { type: Number, required: true },
  volume: { type: Number, required: true},
  adjusted_close: {type: Number, required: false},                         
  change_percent: {type:Number, required: false,},
  avg_vol_20d: { type: Number, required: false,},
  
});

const Tesla = mongoose.model('Tesla', teslaSchema);

module.exports = Tesla;
