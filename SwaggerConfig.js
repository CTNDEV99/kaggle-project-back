const swaggerJsdoc = require('swagger-jsdoc');

const options = {
  swaggerDefinition: {
    info: {
      title: 'API Documentation',
      version: '1.0.0',
      description: 'Documentation for your API',
    },
  },
  apis: ['./Routes/*.js'], // Spécifiez le chemin vers vos fichiers de routes
};

const specs = swaggerJsdoc(options);

module.exports = specs;
