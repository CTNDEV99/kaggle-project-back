const teslaService = require("../Services/TeslaService");

const saveData = async (req, res) => {
  try {
    teslaService.saveData();
    res.status(201).json({ message: "Données enregistrés !" });
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

const getKpi = async (req, res) => {
  teslaService
    .getKpi(req.body.open, req.body.close)
    .then((result) => {
      const results = result[0].results;

      const data = {
        avgHigh: result[0].avgHigh,
        avgLow: result[0].avgLow,
        avgVolume: result[0].avgVolume,
        open: results.map((obj) => obj.open),
        close: results.map((obj) => obj.close),
        date: results.map((obj) => obj.date)
      };

      res.status(201).json(data);
    })
    .catch((error) => {
      res.status(500).json({ error: error });
    });
};
const intDate = async (req, res) => {
  teslaService
    .intDate()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((error) => {
      res.status(500).json({ error: error });
    });
};
module.exports = {
  saveData,
  getKpi,
  intDate,
};
