const Tesla = require("../models/TeslaModel");
const csvFilePath = "./files/tsla_raw_data.csv";
const fs = require("fs");
const csv = require("csv-parser");

const saveData = async () => {
  fs.createReadStream(csvFilePath)
    .pipe(csv())
    .on("data", async (row) => {
      const tesla = new Tesla(row);
      await tesla.save();
    })
    .on("end", () => {});
};
const intDate = async () => {
  try {
    const max = await Tesla.aggregate([
      { $group: { _id: null, maxDate: { $max: "$date" } } },
    ]).exec();

    const min = await Tesla.aggregate([
      { $group: { _id: null, minDate: { $min: "$date" } } },
    ]).exec();

    return { min: min[0].minDate, max: max[0].maxDate };
  } catch (error) {
    throw error;
  }
};
const getKpi = async (start, end) => {
  try {
    const startDate = new Date(start);
    const endDate = new Date(end);

    const results = await Tesla.aggregate([
      {
        $match: {
          date: { $gte: startDate, $lte: endDate },
        },
      },
      {
        $group: {
          _id: null,
          avgHigh: { $avg: "$high" },
          avgLow: { $avg: "$low" },
          avgVolume: { $avg: "$volume" },
          results: { $push: "$$ROOT" },
        },
      },
    ]).exec();
    return results;
  } catch (error) {
    throw error;
  }
};

module.exports = { saveData, intDate, getKpi };
