const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const teslaRoutes = require('./Routes/TeslaRoutes');
const swaggerUi = require('swagger-ui-express');
const swaggerSpecs = require('./SwaggerConfig');


mongoose.connect('mongodb+srv://root:root@cluster0.nfqg8ww.mongodb.net/tesla', { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('Connexion à MongoDB réussie'))
  .catch(err => console.error('Erreur de connexion à MongoDB', err));

app.set('view engine', 'ejs');

app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));

// Middlewares
app.use(cors());
app.use(bodyParser.json());

// Routes
app.use('/api/tesla',teslaRoutes);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpecs));
const port = 3100;
app.listen(port, () => {
  console.log(`Serveur démarré sur le port ${port}`);
});
